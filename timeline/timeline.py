from SPARQLWrapper import SPARQLWrapper, JSON


class ArtistsTimeline:
    def __init__(self):
        self.sparql = SPARQLWrapper("https://query.wikidata.org/sparql")

    def _concatenate_ids(self, mbids):
        ids = ''
        for mbid in mbids:
            ids = ids + '"' + mbid + '",'
        ids = ids[:len(ids) - 1]
        return ids

    def _get_artists_moments(self, mbids):
        ids = self._concatenate_ids(mbids)
        self.sparql.setQuery("""SELECT ?artistLabel ?inception ?date_of_birth ?start_work_period ?end_work_period WHERE {
              ?artist wdt:P434 ?mbid. FILTER (?mbid in (""" + ids + """)).
              OPTIONAL {?artist wdt:P571 ?inception.}
              OPTIONAL {?artist wdt:P569 ?date_of_birth.}
              OPTIONAL {?artist wdt:P2031 ?start_work_period.}
              OPTIONAL {?artist wdt:P2032 ?end_work_period.}
            SERVICE wikibase:label { bd:serviceParam wikibase:language "en".}} """)
        self.sparql.setReturnFormat(JSON)
        data = self.sparql.query().convert()
        items = data['results']['bindings']

        timeline_data = list()
        for item in items:
            # print(item)
            try:
                timeline_data.append({
                    'label': item['artistLabel']['value'] + ', date_of_birth',
                    'date': item['date_of_birth']['value']
                })
            except Exception:
                None
            try:
                timeline_data.append({
                    'label': item['artistLabel']['value'] + ', inception',
                    'date': item['inception']['value']
                })
            except Exception:
                None

            try:
                timeline_data.append({
                    'label': item['artistLabel']['value'] + ', start_work_period',
                    'date': item['start_work_period']['value']
                })
            except Exception:
                None
            try:
                timeline_data.append({
                    'label': item['artistLabel']['value'] + ', end_work_period',
                    'date': item['end_work_period']['value']
                })
            except Exception:
                None

        return timeline_data

    def _get_artists_work(self, mbids):
        ids = self._concatenate_ids(mbids)
        items = list()
        for id in mbids:
            self.sparql.setQuery("""SELECT ?artistLabel ?albumLabel ?typeLabel ?publication_date WHERE {
                ?artist wdt:P434 ?mbid. FILTER (?mbid in ('""" + id + """')).
                ?album wdt:P175 ?artist ;
                wdt:P577 ?publication_date;
                wdt:P31 ?type.       
                SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }}
                LIMIT 10""")
            self.sparql.setReturnFormat(JSON)
            data = self.sparql.query().convert()
            items.extend(data['results']['bindings'])

        timeline_data = list()
        for item in items:
            timeline_data.append({
                'label': item['artistLabel']['value'] + ', ' + item['albumLabel']['value'] + ': ' + item['typeLabel'][
                    'value'],
                'date': item['publication_date']['value']
            })

        return timeline_data

    def get_artists_timeline(self, mbids):
        data = list()
        data.extend(self._get_artists_moments(mbids))
        data.extend(self._get_artists_work(mbids))
        return data


if __name__ == '__main__':
    b = ArtistsTimeline()
    print(b.get_artists_timeline(["bfcc6d75-a6a5-4bc6-8282-47aec8531818",
                                  "f27ec8db-af05-4f36-916e-3d57f91ecf5e",
                                  "6a997121-0919-482b-a91b-c6fe1b55a4b9",
                                  "012151a8-0f9a-44c9-997f-ebd68b5389f9"]))
