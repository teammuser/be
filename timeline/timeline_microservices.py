from flask import Flask
from flask_restful import Resource, abort, Api, reqparse
import timeline

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('mbids', action='append', location='json')


class ArtistsTimeline(Resource):
    def post(self):
        data = timeline.ArtistsTimeline()
        args = parser.parse_args()
        return data.get_artists_timeline(args['mbids'])


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


# tracks

api.add_resource(ArtistsTimeline, '/artists')

# app.run(debug=True)
app.run(host='0.0.0.0', port=80)
