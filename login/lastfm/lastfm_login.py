from SPARQLWrapper import SPARQLWrapper, JSON, POST
from flask_restful import Resource, Api, reqparse
from flask import Flask
import requests
import hashlib


lastfm_root_url = 'http://ws.audioscrobbler.com/2.0/'
api_key = '2f9c841386fd4e2aa9281a9e81617680'
shared_secret = 'a209f9a96a9b557fe92c826fcb84b4c1'

app = Flask(__name__)
api = Api(app)


parser = reqparse.RequestParser()
parser.add_argument('authToken', type=str, location='json')
parser.add_argument('params', type=dict, location='json')


def get_signature(params):
    sig_string = ''
    for param, val in sorted(params.items()):
        sig_string += param + val
    sig_string += shared_secret
    md = hashlib.md5()
    md.update(bytes(sig_string, 'ascii'))
    return md.hexdigest()


def obtain_session(token):
    params = {'api_key': api_key, 'token': token, 'method': 'auth.getSession'}
    sig = get_signature(params)
    params['api_sig'] = sig
    params['format'] = 'json'

    r = requests.get(lastfm_root_url, params=params)
    return r.json()


def get_new_id():
    sparql = SPARQLWrapper('http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/query')
    sparql.setReturnFormat(JSON)
    query = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
        SELECT * WHERE {
            ?user rdf:type pl:User;
            pl:id ?userId
        }
        ORDER BY desc(?userId)
        LIMIT 1
    """
    sparql.setQuery(query)
    results = sparql.query().convert()['results']['bindings']
    if not len(results):
        return 1
    else:
        return int(results[0]['userId']['value']) + 1


def get_user_info(session):
    params = {
        'method': 'user.getinfo',
        'user': session['name'],
        'api_key': api_key,
        'format': 'json'
    }
    r = requests.get(lastfm_root_url, params=params)
    return r.json()['user']


def create_new_user(session):
    sparql = SPARQLWrapper('http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/update')
    sparql.setReturnFormat(JSON)
    sparql.setRequestMethod(POST)
    new_id = get_new_id()
    session = session['session']
    user_info = get_user_info(session)
    query = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
        INSERT DATA {
            <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist#User%s> rdf:type pl:User;
            pl:id '%s';
            pl:name '%s';
            pl:lastfmId '%s';
            pl:lastfmSession '%s'.
        }
    """ % (new_id, new_id, user_info['name'], user_info['name'], session['key'])
    sparql.setQuery(query)
    sparql.query()
    return new_id


def set_lastfm_data(user_id, session):
    sparql = SPARQLWrapper('http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/update')
    sparql.setReturnFormat(JSON)
    sparql.setRequestMethod(POST)
    session = session['session']
    user_info = get_user_info(session)
    query = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
        INSERT {
            ?user pl:lastfmId '%s';
            pl:lastfmSession '%s';
            pl:lastfmUsername '%s'.
        } 
        WHERE {
            ?user rdf:type pl:User;
            pl:id '%s'.
        }
    """ % (user_info['id'], session['key'], session['name'], user_id)
    sparql.setQuery(query)
    sparql.query()
    return user_id


class LastfmLogin(Resource):
    def post(self, user_id=None):
        args = parser.parse_args()
        session = obtain_session(args['authToken'])
        if not user_id:
            return {'userId': create_new_user(session), 'lastfmToken': session}
        set_lastfm_data(user_id, session)
        return {'userId': user_id, 'lastfmToken': session}


class SignLastfm(Resource):
    def post(self):
        arg = parser.parse_args()
        return get_signature(arg)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


api.add_resource(LastfmLogin, '/', '/<user_id>')
api.add_resource(SignLastfm, '/sign')
# app.run(debug=True)
app.run(host='0.0.0.0', port=80)
