from flask_restful import Resource, Api, reqparse
from SPARQLWrapper import SPARQLWrapper, JSON, POST
from flask import Flask

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('authToken', type=str, location='json', required=True)
parser.add_argument('email', type=str, location='json', required=True)
parser.add_argument('name', type=str, location='json', required=True)
parser.add_argument('id', type=str, location='json', required=True)


def get_users_with_fb(fb_id):
    sparql = SPARQLWrapper('http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/query')
    sparql.setReturnFormat(JSON)
    find_user_query = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
        SELECT * WHERE {
            ?user rdf:type pl:User;
            pl:fbId '%s';
            pl:id ?userId.
        }
        LIMIT 1
        """ % fb_id
    sparql.setQuery(find_user_query)
    if sparql.query().convert()['results']['bindings']:
        return sparql.query().convert()['results']['bindings'][0]
    return None


def get_new_id():
    sparql = SPARQLWrapper('http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/query')
    sparql.setReturnFormat(JSON)
    query = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
        SELECT * WHERE {
            ?user rdf:type pl:User;
            pl:id ?userId
        }
        ORDER BY desc(?userId)
        LIMIT 1
    """
    sparql.setQuery(query)
    results = sparql.query().convert()['results']['bindings']
    if not len(results):
        return 1
    else:
        return int(results[0]['userId']['value']) + 1


def create_new_user(user_data):
    sparql = SPARQLWrapper('http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/update')
    sparql.setReturnFormat(JSON)
    sparql.setRequestMethod(POST)
    new_id = get_new_id()
    query = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
        INSERT DATA {
            <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist#User%s> rdf:type pl:User;
            pl:id '%s';
            pl:name '%s';
            pl:fbId '%s'.
        }
    """ % (new_id, new_id, user_data['name'], user_data['id'])
    sparql.setQuery(query)
    sparql.query()
    return new_id


def set_fb_id(user_id, fb_id):
    sparql = SPARQLWrapper('http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/update')
    sparql.setReturnFormat(JSON)
    sparql.setRequestMethod(POST)
    query = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
        INSERT {
            ?user pl:fbId '%s'.
        } 
        WHERE {
            ?user rdf:type pl:User;
            pl:id '%s'.
        }
    """ % (fb_id, user_id)
    sparql.setQuery(query)
    sparql.query()
    return user_id


class FbLogin(Resource):
    def post(self, user_id=None):
        args = parser.parse_args()
        if not user_id:
            user = get_users_with_fb(args['id'])
            if not user:
                new_user = create_new_user(args)
                return {'userId': new_user}
            set_fb_id(user['userId']['value'], args['id'])
            return {'userId': user['userId']['value']}
        set_fb_id(user_id, args['id'])
        return {'userId': user_id}


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


api.add_resource(FbLogin, '/', '/<string:user_id>')
# app.run(debug=True)
app.run(host='0.0.0.0', port=80)
