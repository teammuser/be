import json
import requests


class LastFm:
    network = None

    def __init__(self):
        self.API_KEY = "b1c333c045984241622edd586b2f0216"
        self.API_SECRET = "4f47abb235e9f600640e9c20ee38c431"

    def get_top_artists(self, limit=None):
        data = requests.get(
            'https://ws.audioscrobbler.com/2.0/?method=chart.gettopartists&api_key={0}&limit={1}&format=json'.format(
                self.API_KEY, limit))
        data = data.json()
        artists = list()
        for raw_artist in data['artists']['artist']:
            artist = {'name': raw_artist['name'], 'mbid': raw_artist['mbid'], 'url': raw_artist['url'],
                      'image': raw_artist['image'][4]['#text']}
            artists.append(artist)
        return artists

    def get_top_tracks(self, limit=None):
        data = requests.get(
            'https://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key={0}&limit={1}&format=json'.format(
                self.API_KEY, limit))
        data = data.json()
        tracks = list()
        for raw_track in data['tracks']['track']:
            track = {'name': raw_track['name'], 'mbid': raw_track['mbid'], 'url': raw_track['url'],
                     'image': raw_track['image'][3]['#text'], 'artist': raw_track['artist']}
            tracks.append(track)
        return tracks

    def get_top_artists_by_country(self, iso_3166_1_country_name, limit=None):
        data = requests.get(
            'https://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country={0}&api_key={1}&limit={2}&format=json'.format(
                iso_3166_1_country_name,
                self.API_KEY, limit))
        data = data.json()
        artists = list()
        for raw_artist in data['topartists']['artist']:
            artist = {'name': raw_artist['name'], 'mbid': raw_artist['mbid'], 'url': raw_artist['url'],
                      'image': raw_artist['image'][4]['#text']}
            artists.append(artist)
        return artists

    def get_top_tracks_by_country(self, iso_3166_1_country_name, limit=None):
        data = requests.get(
            'https://ws.audioscrobbler.com/2.0/?method=geo.gettoptracks&country={0}&api_key={1}&limit={2}&format=json'.format(
                iso_3166_1_country_name, self.API_KEY, limit))
        data = data.json()
        tracks = list()
        for raw_track in data['tracks']['track']:
            track = {'name': raw_track['name'], 'mbid': raw_track['mbid'], 'url': raw_track['url'],
                     'image': raw_track['image'][3]['#text'], 'artist': raw_track['artist']}
            tracks.append(track)
        return tracks

    def search_artists_by_name(self, name, limit=None):
        data = requests.get(
            'https://ws.audioscrobbler.com/2.0/?method=artist.search&artist={0}&api_key={1}&limit={2}&format=json'.format(
                name, self.API_KEY, limit))
        data = data.json()
        artists = list()
        for raw_artist in data['results']['artistmatches']['artist']:
            artist = {'name': raw_artist['name'], 'mbid': raw_artist['mbid'], 'url': raw_artist['url'],
                      'image': raw_artist['image'][4]['#text']}
            artists.append(artist)
        return artists

    def search_tracks(self, name, artist_name=None, limit=None):
        if artist_name:
            request = requests.get(
                'https://ws.audioscrobbler.com/2.0/?method=track.search&track={0}&artist={1}&api_key={2}&limit={3}&format=json'.format(
                    name, artist_name, self.API_KEY, limit))
        else:
            request = requests.get(
                'https://ws.audioscrobbler.com/2.0/?method=track.search&track={0}&api_key={1}&limit={2}&format=json'.format(
                    name, self.API_KEY, limit))
        data = request.json()
        tracks = list()
        # print(data['toptracks']['track'])
        for raw_track in data['results']['trackmatches']['track']:
            try:
                mbid = raw_track['mbid']
            except Exception:
                mbid = ''
            track = {'name': raw_track['name'], 'mbid': mbid, 'url': raw_track['url'],
                     'image': raw_track['image'][3]['#text'], 'artist': raw_track['artist']}
            tracks.append(track)
        return tracks

    def get_artist_top_tracks(self, music_brainz_id, limit=None):
        data = requests.get(
            'https://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&mbid={0}&api_key={1}&limit={2}&format=json'.format(
                music_brainz_id, self.API_KEY, limit))
        data = data.json()
        tracks = list()
        for raw_track in data['toptracks']['track']:
            try:
                mbid = raw_track['mbid']
            except Exception:
                mbid = ''
            track = {'name': raw_track['name'], 'mbid': mbid, 'url': raw_track['url'],
                     'image': raw_track['image'][3]['#text']}
            tracks.append(track)
        return tracks

    def get_similar_artists(self, music_brainz_id, limit=None):
        data = requests.get(
            'https://ws.audioscrobbler.com/2.0/?method=artist.getsimilar&mbid={0}&api_key={1}&limit={2}&format=json'.format(
                music_brainz_id, self.API_KEY, limit))
        data = data.json()
        artists = list()
        for raw_artist in data['similarartists']['artist']:
            try:
                mbid = raw_artist['mbid']
            except Exception:
                mbid = ''
            artist = {'name': raw_artist['name'], 'mbid': mbid, 'url': raw_artist['url'],
                      'image': raw_artist['image'][4]['#text']}
            artists.append(artist)
        return artists

    def get_similar_tracks(self, music_brainz_id, limit=None):
        data = requests.get(
            'https://ws.audioscrobbler.com/2.0/?method=track.getsimilar&mbid={0}&api_key={1}&limit={2}&format=json'.format(
                music_brainz_id, self.API_KEY, limit))
        data = data.json()
        tracks = list()
        for raw_track in data['similartracks']['track']:
            try:
                mbid = raw_track['mbid']
            except Exception:
                mbid = ''
            track = {'name': raw_track['name'], 'mbid': mbid, 'url': raw_track['url'],
                     'image': raw_track['image'][3]['#text'],
                     'artist': raw_track['artist']}
            tracks.append(track)
        return tracks

    def search_album_by_name(self, album_name, limit=None):
        data = requests.get(
            'https://ws.audioscrobbler.com/2.0/?method=album.search&album={0}&api_key={1}&limit={2}&format=json'.format(
                album_name,
                self.API_KEY,
                limit
            ))
        data = data.json()
        albums = list()
        for raw_album in data['results']['albummatches']['album']:
            try:
                mbid = raw_album['mbid']
            except Exception:
                mbid = ''
            album = {'name': raw_album['name'], 'mbid': mbid, 'url': raw_album['url'],
                     'image': raw_album['image'][-1]['#text'],
                     'artist': raw_album['artist']}
            albums.append(album)
        return albums

    def get_album_info(self, music_brainz_id):
        data = requests.get(
            'https://ws.audioscrobbler.com/2.0/?method=album.getinfo&mbid={0}&api_key={1}&limit={2}&format=json'.format(
                music_brainz_id,
                self.API_KEY,
            ))
        data = data.json()
        return data

    def get_track_info(self, track_id):
        url = 'https://ws.audioscrobbler.com/2.0'
        params = {
            'method': 'track.getInfo',
            'api_key': self.API_KEY,
            'format': 'json',
            'mbid': track_id
        }
        data = requests.get(url, params=params).json()
        genre = list(map(lambda x: x['name'], data['track']['toptags']['tag']))
        result = {
            'name': data['track']['name'],
            'artist': data['track']['artist']['mbid'],
            'id': data['track']['mbid'],
            'genre': genre
        }
        return result

    def get_artist_info(self, artist_id):
        url = 'https://ws.audioscrobbler.com/2.0'
        params = {
            'method': 'artist.getInfo',
            'api_key': self.API_KEY,
            'format': 'json',
            'mbid': artist_id
        }
        data = requests.get(url, params=params).json()
        result = {
            'name': data['artist']['name'],
            'id': data['artist']['mbid']
        }
        return result


if __name__ == '__main__':
    # r = requests.get(
    #     'https://ws.audioscrobbler.com/2.0/?method=chart.gettopartists&api_key={0}&limit={1}&format=json'.format(
    #         "b1c333c045984241622edd586b2f0216", 4))
    # print(r.content)
    last = LastFm()
    # print(last.search_artists_by_name('James Bay', limit=10))
    print(last.get_similar_tracks("4a6dc20b-0110-47ec-824a-dfcbc887dc23",limit=10))