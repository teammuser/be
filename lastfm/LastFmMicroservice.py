from flask import Flask
from flask_restful import Resource, abort, Api, reqparse
import LastFm

app = Flask(__name__)
api = Api(app)


def abort_if_limit_is_greater_than_fifty(limit):
    if limit:
        if limit > 50:
            abort(400, message="Limit exceeded. Maximum 50 items per request are allowed to be retrieved")


def abort_if_name_is_null_or_empty(name):
    if not name:
        abort(400, message="Name is required")


def abort_if_name_is_not_null_or_empty(name):
    if name:
        abort(400, message="No parameter 'name' allowed")


def abort_if_artist_is_null_or_empty(artist):
    if not artist:
        abort(400, message="Artist name is required")


parser = reqparse.RequestParser()
parser.add_argument('name', type=str, location='args')
parser.add_argument('limit', type=int, location='args')
parser.add_argument('country', type=str, location='args')
parser.add_argument('artist', type=str, location='args')


class ArtistsSearch(Resource):
    def get(self):
        data = LastFm.LastFm()
        args = parser.parse_args()
        return data.search_artists_by_name(args['name'], args['limit'])


class ArtistsTopList(Resource):
    def get(self):
        last_fm = LastFm.LastFm()
        args = parser.parse_args()
        if args['country']:
            return last_fm.get_top_artists_by_country(args['country'], args['limit'])

        return last_fm.get_top_artists(args['limit'])


class ArtistTopTracks(Resource):
    def get(self, mbid):
        data = LastFm.LastFm()
        args = parser.parse_args()
        # abort_if_limit_is_greater_than_thirty(args['limit'])
        return data.get_artist_top_tracks(mbid, args['limit'])


class ArtistsSimilar(Resource):
    def get(self, mbid):
        data = LastFm.LastFm()
        args = parser.parse_args()
        abort_if_limit_is_greater_than_fifty(args['limit'])
        return data.get_similar_artists(mbid, args['limit'])


class ArtistInfo(Resource):
    def get(self, artist_id):
        data = LastFm.LastFm()
        return data.get_artist_info(artist_id)


class TracksSearch(Resource):
    def get(self):
        data = LastFm.LastFm()
        args = parser.parse_args()
        abort_if_limit_is_greater_than_fifty(args['limit'])
        abort_if_name_is_null_or_empty(args['name'])
        return data.search_tracks(args['name'], args['artist'], args['limit'])


class TracksTopList(Resource):
    def get(self):
        data = LastFm.LastFm()
        args = parser.parse_args()
        abort_if_name_is_not_null_or_empty(args['name'])
        abort_if_limit_is_greater_than_fifty(args['limit'])
        if not args['country']:
            return data.get_top_tracks(args['limit'])
        else:
            return data.get_top_tracks_by_country(args['country'], args['limit'])


class TrackGetInfo(Resource):
    def get(self, song_id):
        data = LastFm.LastFm()
        return data.get_track_info(song_id)


class TracksSimilar(Resource):
    def get(self, mbid):
        data = LastFm.LastFm()
        args = parser.parse_args()
        # abort_if_limit_is_greater_than_thirty(args['limit'])
        return data.get_similar_tracks(mbid, args['limit'])


class AlbumList(Resource):
    def get(self):
        last_fm = LastFm.LastFm()
        args = parser.parse_args()
        abort_if_limit_is_greater_than_fifty(args['limit'])
        return last_fm.search_album_by_name(args['name'], args['limit'])


class AlbumInfo(Resource):
    def get(self, mbid):
        last_fm = LastFm.LastFm()
        return last_fm.get_album_info(mbid)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


# artists
api.add_resource(ArtistsSearch, '/artists/search')
api.add_resource(ArtistsTopList, '/artists/top')
api.add_resource(ArtistsSimilar, '/artists/<string:mbid>/similar')
api.add_resource(ArtistTopTracks, '/artists/<string:mbid>/topTracks')
api.add_resource(ArtistInfo, '/artists/<string:artist_id>/info')

# tracks
api.add_resource(TracksSearch, '/tracks/search')
api.add_resource(TracksTopList, '/tracks/top')
api.add_resource(TrackGetInfo, '/tracks/<string:song_id>/info')
api.add_resource(TracksSimilar, '/tracks/<string:mbid>/similar')

# albums
api.add_resource(AlbumList, '/albums/search')
api.add_resource(AlbumInfo, '/albums/<string:mbid>/info')

# app.run(debug=True)
app.run(host='0.0.0.0', port=80)
