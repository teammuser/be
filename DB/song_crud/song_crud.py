from flask import Flask
from flask_restful import Api, Resource, reqparse
from SPARQLWrapper import SPARQLWrapper, POST, JSON
import requests

prefix = '''
    PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
'''
update_db = SPARQLWrapper('http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/update')
update_db.setMethod(POST)
update_db.setReturnFormat(JSON)
query_db = SPARQLWrapper('http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/query')
query_db.setReturnFormat(JSON)

app = Flask(__name__)
api = Api(app)


def get_song_data(song_id):
    url = 'http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/api/lastfm/tracks/%s/info' % song_id
    return requests.get(url).json()


def get_artist_data(artist_id):
    url = 'http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/api/lastfm/artists/%s/info' % artist_id
    return requests.get(url).json()


def get_artist_uri(artist_id):
    url = 'http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/api/artists/%s' % artist_id
    data = requests.get(url).json()
    if data.get('errMsg', None):
        data = requests.post(url).json()
    return data['uri']


def get_tags_insert(tags):
    response = '''pl:genre '%s' ''' % tags[0]
    for i in range(1, len(tags)):
        response += ''';
        pl:genre '%s' ''' % tags[i]
    return response


class SongCRUD(Resource):
    def get(self, song_id):
        query = ''' %s
            SELECT * WHERE {
                ?song rdf:type pl:Song;
                    pl:id '%s';
                    pl:name ?name.
                OPTIONAL{?song pl:hasArtist ?artist.}.
                OPTIONAL{?song pl:duration ?duration.}.
            }
        ''' % (prefix, song_id)
        query_db.setQuery(query)
        raw_response = query_db.query().convert()
        if len(raw_response['results']['bindings']) == 0:
            return {'errMsg': "No song with the specified ID"}, 404
        raw_response = raw_response['results']['bindings'][0]
        raw_response['artist'] = raw_response['artist']['value']
        raw_response['name'] = raw_response['name']['value']
        raw_response['uri'] = raw_response['song']['value']
        raw_response['id'] = song_id
        raw_response.pop('song', None)
        return raw_response

    def post(self, song_id):
        song = get_song_data(song_id)
        artist = get_artist_data(song['artist'])
        artist_uri = get_artist_uri(artist['id'])
        query = ''' %s
            INSERT DATA {
                <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist#Song%s> rdf:type pl:Song;
                    pl:id '%s';
                    pl:name '%s';
                    pl:hasArtist <%s>;
                    %s.
            }
        ''' % (prefix, song_id, song_id, song['name'], artist_uri, get_tags_insert(song['genre']))
        update_db.setQuery(query)
        update_db.query()
        song['uri'] = 'http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist#Song%s' % song_id
        return song, 201


api.add_resource(SongCRUD, '/<string:song_id>')
# app.run(debug=True)
app.run(host='0.0.0.0', port=80)
