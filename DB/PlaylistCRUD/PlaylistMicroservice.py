from flask import Flask
from flask_restful import Api, Resource, reqparse
import PlaylistRepository

app = Flask(__name__)
api = Api(app)

req = reqparse.RequestParser()
req.add_argument('songs', location='json', action='append')
# req.add_argument('userId', type=str, location='args')
req.add_argument('name', type=str, location='json')


class PlaylistCRUD(Resource):
    def __init__(self):
        self.playlistManager = PlaylistRepository.PlaylistsRepository()

    def get(self, user_id, playlist_id=None):
        if playlist_id:
            return self.playlistManager.get_playlist_data(playlist_id)
        else:
            return self.playlistManager.get_all_playlists(user_id)

    def post(self, user_id, playlist_id=None):
        args = req.parse_args()
        if not args.get('songs', None):
            args['songs'] = []
        if playlist_id:
            self.playlistManager.add_songs_to_playlist(args['songs'], playlist_id, args['name'])
            return {'id': playlist_id}, 201
        new_id = self.playlistManager.new_playlist(args['name'], user_id, args['songs'])
        return {'id': new_id}, 201


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


api.add_resource(PlaylistCRUD, '/<string:user_id>', '/<string:user_id>/<string:playlist_id>')
# app.run(debug=True)
app.run(host='0.0.0.0', port=80)
