from SPARQLWrapper import SPARQLWrapper, POST, JSON
import requests


def get_songs_insert_string(songs):
    if not len(songs):
        return None
    result = 'pl:hasSong <%s>' % songs[0]
    for i in range(1, len(songs)):
        result += ''';
        pl:hasSong <%s>''' % songs[i]
    return result


def verify_song(song):
    url = 'http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/api/songs/%s' % song
    data = requests.get(url).json()
    if data.get('errMsg', None):
        data = requests.post(url).json()
    return data['uri']


def prettify_playlist(data):
    p = {
        'name': data['name']['value'],
        'id': data['playlistId']['value']
    }
    return p


class PlaylistsRepository:
    prefix = '''PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
           PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>'''

    def __init__(self):
        self.update_db = SPARQLWrapper(
            'http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/update')
        self.update_db.setMethod(POST)
        self.query_db = SPARQLWrapper(
            'http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/query')
        self.query_db.setReturnFormat(JSON)

    def get_new_id(self):
        query = """ %s
            SELECT * WHERE {
                ?playlist rdf:type pl:Playlist;
                    pl:id ?playlistId.
            }
            ORDER BY desc(?playlistId)
            LIMIT 1
        """ % self.prefix
        self.query_db.setQuery(query)
        results = self.query_db.query().convert()['results']['bindings']
        if not len(results):
            return 1
        else:
            return int(results[0]['playlistId']['value']) + 1

    def new_playlist(self, playlist_name, user_id, song_ids):
        playlist_id = self.get_new_id()
        query = '''%s
            INSERT {
                <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist#Playlist%s> rdf:type pl:Playlist;
                    pl:id '%s';
                    pl:name '%s'.
                ?user pl:hasPlaylist <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist#Playlist%s>.
            } WHERE {
                ?user rdf:type pl:User;
                    pl:id '%s'.
            }
        ''' % (self.prefix, playlist_id, playlist_id, playlist_name, playlist_id, user_id)
        self.update_db.setQuery(query)
        self.update_db.query()
        self.add_songs_to_playlist(song_ids, playlist_id)
        return playlist_id

    def add_to_playlist(self, playlist_id, songs_uri):
        song_insert_str = get_songs_insert_string(songs_uri)
        if not song_insert_str:
            return None
        query = '''%s
        INSERT {
            ?playlist %s.
        } WHERE {
            ?playlist pl:id '%s';
                rdf:type pl:Playlist.
        }''' % (self.prefix, song_insert_str, playlist_id)
        self.update_db.setQuery(query)
        self.update_db.query().convert()

    def add_songs_to_playlist(self, songs, playlist_id, name=None):
        song_uris = list(map(verify_song, songs))
        self.add_to_playlist(playlist_id, song_uris)
        if name:
            query = '''%s
                DELETE {
                    ?name
                } WHERE {
                    ?playlist rdf:type pl:Playlist;
                        pl:id '%s';
                        pl:name ?name.
                }
            ''' % (self.prefix, playlist_id)
            self.update_db.setQuery(query)
            self.update_db.query().convert()
            query = '''%s
                INSERT {
                    ?playlist pl:name '%s'.
                } WHERE {
                    ?playlist rdf:type pl:Playlist;
                        pl:id '%s'.
                }
            ''' % (self.prefix, name, playlist_id)
            self.update_db.setQuery(query)
            self.update_db.query().convert()

    def get_playlist_songs(self, playlist_id):
        query = '''%s
            SELECT ?songId WHERE {
                ?playlist rdf:type pl:Playlist
                    pl:id '%s';
                    pl:hasSong ?song.
                ?song pl:id ?songId.
                ?song rdf:type pl:Song.
            }
        ''' % (self.prefix, playlist_id)
        self.query_db.setQuery(query)
        return self.query_db.query().convert()['results']['bindings']

    def get_playlist_data(self, playlist_id):
        query = '''%s
            SELECT * WHERE {
                ?playlist rdf:type pl:Playlist;
                    pl:id '%s';
                    pl:name ?playlistName;
                    pl:hasSong ?song.
                ?song pl:id ?songId;
                    pl:name ?songName;
                    pl:hasArtist ?artist.
                ?artist pl:name ?artistName;
                    pl:id ?artistId.
            }
        ''' % (self.prefix, playlist_id)
        self.query_db.setQuery(query)
        raw_result = self.query_db.query().convert()['results']['bindings']
        if not raw_result:
            return None
        result = raw_result[0]
        result['name'] = result['playlistName']['value']
        result['uri'] = result['playlist']['value']
        result['id'] = playlist_id
        result['songs'] = []
        for i in range(len(raw_result)):
            aux = {
                'name': raw_result[i]['songName']['value'],
                'id': raw_result[i]['songId']['value'],
                'artist': {
                    'name': raw_result[i]['artistName']['value'],
                    'id': raw_result[i]['artistId']['value']
                }
            }
            result['songs'].append(aux)
        result.pop('playlist', None)
        result.pop('playlistName', None)
        result.pop('song', None)
        result.pop('songName', None)
        result.pop('songId', None)
        result.pop('artist', None)
        result.pop('artistName', None)
        result.pop('artistId', None)
        return result

    def get_all_playlists(self, user_id):
        query = '''%s
            SELECT * WHERE {
                ?user rdf:type pl:User;
                      pl:id '%s';
                      pl:hasPlaylist ?playlist.
                ?playlist pl:id ?playlistId;
                    pl:name ?name.
            }
        ''' % (self.prefix, user_id)
        self.query_db.setQuery(query)
        data = self.query_db.query().convert()['results']['bindings']
        data = list(map(prettify_playlist, data))
        return data
