from flask import Flask
from flask_restful import Api, Resource, reqparse
from SPARQLWrapper import SPARQLWrapper, POST, JSON

import requests

prefix = '''
    PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
'''
update_db = SPARQLWrapper('http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/update')
update_db.setMethod(POST)
update_db.setReturnFormat(JSON)
query_db = SPARQLWrapper('http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/query')
query_db.setReturnFormat(JSON)

app = Flask(__name__)
api = Api(app)


def get_artist_data(artist_id):
    url = 'http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/api/lastfm/artists/%s/info' % artist_id
    return requests.get(url).json()


def check_if_artist_exists(artist_id):
    query = ''' %s
               SELECT * WHERE {
                   ?song rdf:type pl:Artist;
                       pl:id '%s';
                       pl:name ?name;
                       pl:hasSong ?song.                
               }
           ''' % (prefix, artist_id)
    query_db.setQuery(query)
    raw_response = query_db.query().convert()
    if len(raw_response['results']['bindings']) == 0:
        return False
    else:
        return True


class ArtistCrud(Resource):
    def get(self, artist_id):
        query = ''' %s
            SELECT * WHERE {
                ?artist rdf:type pl:Artist;
                    pl:id '%s';
                    pl:name ?name.               
            }
        ''' % (prefix, artist_id)
        query_db.setQuery(query)
        raw_response = query_db.query().convert()
        if len(raw_response['results']['bindings']) == 0:
            return {'errMsg': "No artist with the specified ID"}, 404
        raw_response = raw_response['results']['bindings'][0]
        response = dict()
        response['artist'] = raw_response['artist']['value']
        response['name'] = raw_response['name']['value']
        response['uri'] = raw_response['artist']['value']
        response['id'] = "%s" % artist_id
        return response

    def post(self, artist_id):
        artist = get_artist_data(artist_id)
        is_duplicate = check_if_artist_exists(artist['id'])
        if is_duplicate:
            return {"errMsg:": "The artist with the specified id already exists in the db"}, 500
        query = ''' %s
            INSERT DATA {
                <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist#Artist%s> rdf:type pl:Artist;
                    pl:id '%s';
                    pl:name '%s';
            }
        ''' % (prefix, artist_id, artist_id, artist['name'])
        update_db.setQuery(query)
        update_db.query()
        artist['uri'] = 'http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist#Artist%s' % artist_id
        return artist, 201


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


api.add_resource(ArtistCrud, '/<string:artist_id>')
# app.run(debug=True)
app.run(host='0.0.0.0', port=80)
