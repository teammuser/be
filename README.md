# Muser BE
The aim of this repository is to provide microservices for retrieving music artists and songs related data.

## Login usage

### For login with facebook

POST : `/login/fb/<user_id?>`
  - body parameters are required: `authToken`, `email`, `name`, `id`
  - the user_id is optional, if provided the fb account will be paired with the user with the provided id
  - it will be returned the id of the user (even if the id is provided)
  - if an id is not provided and there is no user which has paired to his account a facebook account, a new user will be created

POST : `login/lastfm/<user_id?>`

POST : `lastfm/sign`
  - in the body there needs to be an object that contains an object called params that is constructed this way: the key is the parameter name and the value is the parameter value, ex: 
  { 
    'params': { 
        'authToken': < value of the auth token > 
    } 
  }
  - returned will be the signature for lastfm signed API calls

## Lastfm microservices usage guideline  
In the case of all exposed methods, there is a `limit` optional parameter, that allows to set the number of items to be retrived in the request. In the case that `limit` is not set, the LastFm default is used.
The maximum value that the `limit` parameter can have is 30.  
#### Artist end points:  
search: `/artist?name=artist_name&limit=limit`; `name` parameter is required   
retrive top: `/artists?country=iso_3166_1_country_name&limit=limit`; `country` paramater is optional  
retrive similar artists: `/artist/similar?name=artist_name&limit=limit`; `name` parameter is required  
retrive artist top tracks:  `/artist/tracks?name=artist_name&limit=limit`; `name` parameter is required  

#### Tracks end points:  
Path are identical to that of the artists, but instead of`/artist` or `/artists` is needed `/track`, `/tracks`  
Retrival of similar tracks is diferent. An artist name is required along with the name of the tracks: `/tracks/similar?name=song_name&artist=artis_name&limit=limit`  

#### Methods retrival:

All methods retrive data in json format. The main key is the scope of the method. For example, the similar artist method will retrive a json with `similarartists` as main key.
The next important inner key is the entity type (track/artist) for which we can get the name, the artist name (if case), an image( from an array of url images) and the url to last fm.

## Wikidata microservices usage guideline  

#### Discography end points
`/discograpy/music_brainz_id` will retrive the discography of an artist/music band by using it's MusicBrainz artist ID. The method will retrive data in json format.
The main data is retrived unde ['results']['bidings'] key, and information about individual items can be accessed throw `artistLabel`, `musicalWorkLabel`, `publicationDate`. 
