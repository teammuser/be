import musicbrainzngs as m
import muser_classes as muser


class MusicBrainz:

    def __init__(self):
        m.set_useragent("MUSICBRAINZ_SERVICE", "0.1", "http://musicbrainz.com")
        m.auth("", "")

    def get_artist_songs_genres(artist_name):
        artists = m.search_artists(artist=artist_name, strict=True)
        artist_props = artists['artist-list']
        print(artist_props[0]['tag-list'])

        artist_songs_genres = list()
        for x in artist_props[0]['tag-list']:
            artist_songs_genres.append(x['name'])

        return artist_songs_genres

    def get_artists_name_and_id_suggestion(search_text, offset):
        if offset == 0:
            artists = m.search_artists(artist=search_text)
        else:
            artists = m.search_artists(artist=search_text, offset=next)

        artists = artists['artist-list']
        for art in artists:
            print(art)

    def search_artists_by_name(self, name):
        artists = m.search_artists(artist=name, country='US')
        return artists

    def search_artist(self, artist_name):
        data = m.search_artists(artist=artist_name, strict=True)
        print(data['artist-list'])


if __name__ == '__main__':
    musicBrainz = MusicBrainz()

    musicBrainz.search_artist('James Bay')
