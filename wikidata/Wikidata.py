from SPARQLWrapper import SPARQLWrapper, JSON


class Wikidata:
    def __init__(self):
        self.sparql = SPARQLWrapper("https://query.wikidata.org/sparql")

    def get_artist_discography(self, artist_music_brainz_id):
        self.sparql.setQuery("""

                 SELECT ?artistLabel ?musicalWorkLabel ?publicationDate WHERE{{
                  select distinct ?artist 
                                  where {
                    ?artist wdt:P434  '""" + artist_music_brainz_id + """'.
                  }}
                  ?musicalWork wdt:P175 ?artist;
                         wdt:P577 ?publicationDate.
                    ?musicalWork wdt:P31 ?type
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
                }
                ORDER By ?publicationDate
        """)
        self.sparql.setReturnFormat(JSON)
        return self.sparql.query().convert()

    def get_similar_artists_by_genre(self, artist_music_brainz_id, limit):
        self.sparql.setQuery("""
            SELECT ?similarArtist ?similarArtistLabel ?musicBrainzId ?image where {
            {SELECT ?genre
                WHERE {
                ?artist wdt:P434 '"""+ str(artist_music_brainz_id)+"""'.
                ?artist wdt:P136 ?genre.
            }}
            ?similarArtist wdt:P136 ?genre;
                         wdt:P434 ?musicBrainzId.
            OPTIONAL {?similarArtist wdt:P18 ?image}
            SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        }
        LIMIT """ + str(limit))
        self.sparql.setReturnFormat(JSON)
        return self.sparql.query().convert()
