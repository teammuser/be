from flask import Flask
from flask_restful import Resource, abort, Api, reqparse
import Wikidata

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('limit', type=int, location='args')


class ArtistDiscography(Resource):
    def get(self, music_brainz_id):
        wiki = Wikidata.Wikidata()
        data = wiki.get_artist_discography(music_brainz_id)
        return data


class ArtistsSimilarByGenre(Resource):
    def get(self, music_brainz_id):
        wiki = Wikidata.Wikidata()
        args = parser.parse_args()
        data = wiki.get_similar_artists_by_genre(music_brainz_id, args['limit'])
        return data


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


api.add_resource(ArtistDiscography, '/artist/<string:music_brainz_id>/discography')
api.add_resource(ArtistsSimilarByGenre, '/artists/<string:music_brainz_id>/similar')


# app.run(debug=True, host='0.0.0.0', port=80)
app.run(host='0.0.0.0', port=80)
