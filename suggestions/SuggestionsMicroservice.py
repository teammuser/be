from flask import Flask
from flask_restful import Resource, abort, Api, reqparse
import Suggestions

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('userId', type=str, location='args')


class TracksSuggestions(Resource):
    def get(self):
        args = parser.parse_args()
        data = Suggestions.TracksSuggestions()
        return data.get_tracks_suggestions_by_tracks_ids(args['userId'])


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


# tracks

api.add_resource(TracksSuggestions, '/tracks')

# app.run(debug=True)
app.run(host='0.0.0.0', port=80)
