from SPARQLWrapper import SPARQLWrapper, JSON
import requests
import random

API_KEY = "b1c333c045984241622edd586b2f0216"


def get_top_tracks(music_brainz_id, limit=None):
    data = requests.get(
        'https://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&mbid={0}&api_key={1}&limit={2}&format=json'.format(
            music_brainz_id, API_KEY, limit))
    data = data.json()
    tracks = list()
    for raw_track in data['toptracks']['track']:
        try:
            mbid = raw_track['mbid']
        except Exception:
            mbid = ''
        track = {'name': raw_track['name'], 'mbid': mbid, 'url': raw_track['url'],
                 'image': raw_track['image'][3]['#text']}
        tracks.append(track)
    return tracks


def get_similar_tracks_shel( music_brainz_id, limit=None):
    data = requests.get(
        'https://ws.audioscrobbler.com/2.0/?method=track.getsimilar&mbid={0}&api_key={1}&limit={2}&format=json'.format(
            music_brainz_id, API_KEY, limit))
    data = data.json()
    tracks = list()
    for raw_track in data['similartracks']['track']:
        try:
            mbid = raw_track['mbid']
        except Exception:
            mbid = ''
        track = {'name': raw_track['name'], 'mbid': mbid, 'url': raw_track['url'],
                 'image': raw_track['image'][3]['#text'],
                 'artist': raw_track['artist']}
        tracks.append(track)
    return tracks


class Similar:
    def __init__(self, obj):
        self.obj = obj

    def __eq__(self, other):
        return self.obj['mbid'] == other.obj['mbid']

    def __hash__(self):
        return hash(self.obj['mbid'])


class TracksSuggestions:

    def __init__(self):
        self.sparql = SPARQLWrapper(
            "http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/sparql/Playlist-Test/query")

    def get_playlist_tracks_ids(self, user_id):
        self.sparql.setQuery("""
            PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            SELECT ?mbid WHERE {
              ?playlist rdf:type pl:Playlist;
                        pl:hasSong ?song.
              ?song pl:id ?mbid.
              ?user rdf:type pl:User;
                    pl:id '""" + user_id + """';
                    pl:hasPlaylist ?playlist.
            }
                """)
        self.sparql.setReturnFormat(JSON)
        data = self.sparql.query().convert()
        return data['results']['bindings']

    def get_playlist_artists_ids(self):
        self.sparql.setQuery("""
        PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        SELECT ?mbid WHERE {
          ?playlist rdf:type pl:Playlist.
          ?playlist pl:hasSong ?song.
          ?song pl:hasArtist ?artist.
                ?artist pl:id ?mbid.
        }
        """)
        self.sparql.setReturnFormat(JSON)
        data = self.sparql.query().convert()
        return data['results']['bindings']

    def get_similar_tracks(self, limit, tracks_ids):
        similar_tracks = list()
        params = {'limit': limit}
        for mbid in tracks_ids:
            r = get_similar_tracks_shel(mbid['mbid']['value'], 20)
            # r = requests.get(
            #     'http://http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/api/lastfm/tracks/{0}/similar'.format(mbid['mbid']['value']),
            #     params=params)
            similar_tracks.extend(r)
        return similar_tracks

    def get_artist_top_tracks(self, limit, artists_ids):
        top_tracks = list()
        params = {'limit': limit}
        for mbid in artists_ids:
            r = get_top_tracks(mbid['mbid']['value'], limit=20)
            # r = requests.get(
            #     'http://http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/api/lastfm/artist/{0}/topTracks'.format(mbid['mbid']['value']),
            #     params=params)
            new_tracks = list()
            for track in r:
                track.update({'artist': {'mbid': mbid['mbid']['value']}})
                new_tracks.append(track)
            top_tracks.extend(new_tracks)
        return top_tracks

    def get_tracks_suggestions_by_artists_ids(self):
        artists_ids = self.get_playlist_artists_ids()
        number_of_artists = len(artists_ids)
        if 1 <= number_of_artists <= 4:
            similar_tracks = self.get_artist_top_tracks(15, artists_ids)
        elif 5 <= number_of_artists <= 10:
            similar_tracks = self.get_artist_top_tracks(8, artists_ids)
        elif 11 <= number_of_artists <= 49:
            similar_tracks = self.get_artist_top_tracks(5, artists_ids)
        else:
            similar_tracks = self.get_artist_top_tracks(1, artists_ids)
        if len(similar_tracks) >= 25:
            tracks = random.choices((list(set([Similar(x) for x in similar_tracks]))), k=25)
        else:
            tracks = []

        result = list()
        for t in tracks:
            new_t = {'name': t.obj['name'], 'mbid': t.obj['mbid'], 'url': t.obj['url'],
                     'image': t.obj['image']}
            result.append(new_t)
        return result

    def get_tracks_suggestions_by_tracks_ids(self, user_id):
        tracks_ids = self.get_playlist_tracks_ids(user_id)
        number_of_tracks = len(tracks_ids)
        if 1 <= number_of_tracks <= 4:
            similar_tracks = self.get_similar_tracks(35, tracks_ids)
        elif 5 <= number_of_tracks <= 10:
            similar_tracks = self.get_similar_tracks(20, tracks_ids)
        elif 11 <= number_of_tracks <= 49:
            similar_tracks = self.get_similar_tracks(10, tracks_ids)
        elif 50 <= number_of_tracks <= 99:
            similar_tracks = self.get_similar_tracks(5, tracks_ids)
        else:
            similar_tracks = self.get_similar_tracks(1, tracks_ids)
        if len(similar_tracks) >= 25:
            tracks = random.choices((list(set([Similar(x) for x in similar_tracks]))), k=25)
        else:
            tracks = []

        result = list()
        for t in tracks:
            new_t = {'name': t.obj['name'], 'mbid': t.obj['mbid'], 'url': t.obj['url'],
                     'image': t.obj['image'],
                     'artist': t.obj['artist']}
            result.append(new_t)
        return result


if __name__ == '__main__':
    suggestion = TracksSuggestions()
    print(suggestion.get_tracks_suggestions_by_artists_ids())
