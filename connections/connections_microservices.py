from flask import Flask
from flask_restful import Resource, abort, Api, reqparse
import connections_p

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('mbids', action='append', location='json')


class ArtistsConnections(Resource):
    def post(self):
        data = connections_p.ArtistsConnections()
        args = parser.parse_args()
        return data.get_artists_connections(args['mbids'])


class SongsConnections(Resource):
    def post(self):
        data = connections_p.SongsConnections()
        args = parser.parse_args()
        return data.get_nodes(args['mbids'])


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


# artists
api.add_resource(ArtistsConnections, '/artists')

# songs
api.add_resource(SongsConnections, '/tracks')

# app.run(debug=True)
app.run(host='0.0.0.0', port=80)
