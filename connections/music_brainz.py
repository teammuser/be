import musicbrainzngs as m


class MusicBrainz:

    def __init__(self):
        m.set_useragent("MUSICBRAINZ_SERVICE", "0.1", "http://musicbrainz.com")
        m.auth("", "")

    def get_artist_songs_genres(artist_name):
        artists = m.search_artists(artist=artist_name, strict=True)
        artist_props = artists['artist-list']
        print(artist_props[0]['tag-list'])

        artist_songs_genres = list()
        for x in artist_props[0]['tag-list']:
            artist_songs_genres.append(x['name'])

        return artist_songs_genres

    def get_artists_name_and_id_suggestion(search_text, offset):
        if offset == 0:
            artists = m.search_artists(artist=search_text)
        else:
            artists = m.search_artists(artist=search_text, offset=next)

        artists = artists['artist-list']
        for art in artists:
            print(art)

    def search_works(self, artist, track, artist_mbid):
        work = m.search_recordings(tracks=track, arid=artist_mbid)
        result = set()
        for key in work['recording-list']:
            try:
                print(key)
                realease_list = key['release-list']
                for x in realease_list:
                    if x['title'] == track:
                        result.add([x['release-group']['id']])
            except Exception:
                None

        return result


if __name__ == '__main__':
    music = MusicBrainz()
    print(music.search_works('Cher', 'Hold Back The River', '6a997121-0919-482b-a91b-c6fe1b55a4b9'))
