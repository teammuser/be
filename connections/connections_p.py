from SPARQLWrapper import SPARQLWrapper, JSON, XML, POST
import requests

prefix = '''
    PREFIX pl: <http://ec2-18-194-52-35.eu-central-1.compute.amazonaws.com/ontology/playlist>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>'''


class ArtistsConnections:
    def concatenate_ids(self, mbids):
        ids = ''
        for mbid in mbids:
            ids = ids + '"' + mbid + '",'
        ids = ids[:len(ids) - 1]
        return ids

    def get_artists_connection_data(self, mbids):
        ids = self.concatenate_ids(mbids)
        sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
        sparql.setQuery("""
                      Select ?artist ?mbid ?artistLabel ?genre ?genreLabel ?record ?recordLabel ?citizenship ?citizenshipLabel ?placeOfBirth ?placeOfBirthLabel 
             ?inception ?localtionOfFormation ?localtionOfFormationLabel
            WHERE{
              ?artist wdt:P434 ?mbid. Filter ( ?mbid in (""" + ids + """)).
              ?artist wdt:P136 ?genre.
              ?artist wdt:P264 ?record.
              OPTIONAL {?artist wdt:P27 ?citizenship.}
              OPTIONAL {?artist wdt:P19 ?placeOfBirth.}
              OPTIONAL {?artist wdt:P571 ?inception.}
              OPTIONAL {?artist wdt:P740 ?localtionOfFormation.}
              SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
            }
        """)
        sparql.setReturnFormat(JSON)
        return sparql.query().convert()

    def get_edges(self, label, data):
        edges = list()
        for key in data:
            lenght = len(data[key])
            if lenght > 1:
                items = list(data[key])
                for i in range(lenght - 1):
                    for j in range(1, lenght):
                        edges.append({
                            'from': items[i],
                            'to': items[j],
                            'label': label + key
                        })
        return edges

    def get_edges_by_genres(self, data):
        genres = dict()
        for r in data:
            if r['genreLabel']['value'] not in genres:
                genres[r['genreLabel']['value']] = set([r['mbid']['value']])
            else:
                genres[r['genreLabel']['value']].add(r['mbid']['value'])
        return self.get_edges('play music genre: ', genres)

    def get_edges_by_records(self, data):
        records = dict()
        for r in data:
            if r['recordLabel']['value'] not in records:
                records[r['recordLabel']['value']] = set([r['mbid']['value']])
            else:
                records[r['recordLabel']['value']].add(r['mbid']['value'])
        return self.get_edges('recorded songs at: ', records)

    def get_edges_by_inception(self, data):
        inceptions = dict()
        for r in data:
            try:
                inception_year = r['inception']['value'].split('-')[0]
                if inception_year not in inceptions:
                    inceptions[inception_year] = set([r['mbid']['value']])
                else:
                    inceptions[inception_year].add(r['mbid']['value'])
            except Exception:
                None
        return self.get_edges('had same inception year : ', inceptions)

    def get_edges_by_citizenship(self, data):
        citizenships = dict()
        for r in data:
            try:
                citizenship = r['citizenshipLabel']['value']
                if citizenship not in citizenships:
                    citizenships[citizenship] = set([r['mbid']['value']])
                else:
                    citizenships[citizenship].add(r['mbid']['value'])
            except Exception:
                None
        return self.get_edges('have same citizenship : ', citizenships)

    def get_edges_by_location_of_formation(self, data):
        locations = dict()
        for r in data:
            try:
                location_of_formation = r['localtionOfFormationLabel']['value']
                if location_of_formation not in locations:
                    locations[location_of_formation] = set([r['mbid']['value']])
                else:
                    locations[location_of_formation].add(r['mbid']['value'])
            except Exception:
                None
        return self.get_edges('have the same formation location : ', locations)

    def get_edges_by_place_of_birth_and_location_of_formation(self, data):
        locations = dict()
        for r in data:
            try:
                location_of_formation = r['localtionOfFormationLabel']['value']
                if location_of_formation not in locations:
                    locations[location_of_formation] = set([r['mbid']['value']])
                else:
                    locations[location_of_formation].add(r['mbid']['value'])
            except Exception:
                None

        places = list()
        for r in data:
            try:
                place_of_birth = r['placeOfBirthLabel']['value']
                if place_of_birth in locations:
                    for x in locations[place_of_birth]:
                        places.append({
                            'from': r['mbid']['value'],
                            'to': x,
                            'label': 'has same birth place as the location of inception: ' + place_of_birth
                        })
            except Exception:
                None

        return places

    def get_artists(self, mbids):
        ids = self.concatenate_ids(mbids)
        sparql = SPARQLWrapper("https://query.wikidata.org/sparql")

        sparql.setQuery("""
       SELECT ?artistLabel ?mbid WHERE {
              ?artist wdt:P434 ?mbid. FILTER (?mbid in (""" + ids + """)).
           
            SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
            }""")

        sparql.setReturnFormat(JSON)
        data = sparql.query().convert()
        items = data['results']['bindings']

        result = list()
        for item in items:
            result.append({
                'name': item['artistLabel']['value'],
                'id': item['mbid']['value']
            })
        return result

    def get_artists_connections(self, mbids):
        result = \
            self.get_artists_connection_data(mbids)['results']['bindings']
        edges = list()
        edges.extend(self.get_edges_by_genres(result))
        edges.extend(self.get_edges_by_records(result))
        edges.extend(self.get_edges_by_citizenship(result))
        edges.extend(self.get_edges_by_location_of_formation(result))
        edges.extend(self.get_edges_by_inception(result))
        edges.extend(self.get_edges_by_place_of_birth_and_location_of_formation(result))

        artists = self.get_artists(mbids)
        nodes = list(self.get_artists(mbids))

        return {'nodes': nodes, 'edges': edges}


class SongsConnections:
    def __init__(self):
        self.API_KEY = "b1c333c045984241622edd586b2f0216"

    def get_edges(self, label, data):
        edges = list()
        for key in data:
            lenght = len(data[key])
            if lenght > 1:
                items = list(data[key])
                for i in range(lenght - 1):
                    for j in range(1, lenght):
                        edges.append({
                            'from': items[i],
                            'to': items[j],
                            'label': label + key
                        })
        return edges

    def get_song_info(self, mbid):
        url = 'https://ws.audioscrobbler.com/2.0'
        params = {
            'method': 'track.getInfo',
            'api_key': self.API_KEY,
            'format': 'json',
            'mbid': mbid
        }
        data = requests.get(url, params=params).json()
        result = {
            'name': data['track']['name'],
            'artist': data['track']['artist']['mbid'],
            'artist_name': data['track']['artist']['name'],
            'id': data['track']['mbid'],
            'tags': data['track']['toptags']
        }
        return result

    def get_songs_dictionary(self, mbids):
        genres = dict()
        for id in mbids:
            song = self.get_song_info(id)
            top_tags = song['tags']
            for tag in top_tags['tag']:
                if tag['name'] not in genres:
                    genres[tag['name']] = set([song['id']])
                else:
                    genres[tag['name']].add(song['id'])
        return self.get_edges("have the same tags: ", genres)

    def get_artists_dictionary(self, mbids):
        artists = dict()
        for id in mbids:
            song = self.get_song_info(id)
            artist = song['artist_name']
            if artist not in artists:
                artists[artist] = set([song['id']])
            else:
                artists[artist].add(song['id'])
        return self.get_edges("have the same artist: ", artists)

    def get_nodes(self, mbids):
        nodes = list()
        edges = list()
        edges.extend(self.get_songs_dictionary(mbids))
        edges.extend(self.get_artists_dictionary(mbids))

        for id in mbids:
            song = self.get_song_info(id)
            nodes.append({'name': song['name'], 'id': song['id']})
        return {'nodes': nodes, 'edges': edges}
#
#
# if __name__ == '__main__':
#     b = SongsConnections()
#     print(b.get_nodes(['ef8dc9bb-59c9-415b-870d-e628d7dad4c2', 'aa736999-fcfa-406a-aa89-28d1c9c82c8b', '8b5a6bf1-017d-4fea-901b-0ae8d3c4204a']))
# #
# if __name__ == '__main__':
#     b = ArtistsConnections()
#     print(b.get_artists_connections(["bfcc6d75-a6a5-4bc6-8282-47aec8531818",
#                                      "f27ec8db-af05-4f36-916e-3d57f91ecf5e",
#                                      "6a997121-0919-482b-a91b-c6fe1b55a4b9",
#                                      "012151a8-0f9a-44c9-997f-ebd68b5389f9"]))
