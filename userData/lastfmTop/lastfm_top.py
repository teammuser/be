from flask_restful import Resource, Api, reqparse
from flask import Flask
import requests

lastfm_root_url = 'http://ws.audioscrobbler.com/2.0/'
api_key = '2f9c841386fd4e2aa9281a9e81617680'
shared_secret = 'a209f9a96a9b557fe92c826fcb84b4c1'

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('authToken', type=str, location='json')


def strip_usseles_stuf(song):
    song['image'] = song['image'][-1]['#text']
    song.pop('streamable', None)
    song.pop('@attr', None)
    song.pop('playcount', None)
    return song


class LastfmTopSongs(Resource):
    def get(self, user_id):
        params = {
            'format': 'json',
            'user': user_id,
            'api_key': api_key,
            'method': 'user.gettoptracks'
        }
        result = requests.get(lastfm_root_url, params=params).json()['toptracks']['track']
        result = list(map(strip_usseles_stuf, result))
        return result


class LastfmTopArtists(Resource):
    def get(self, user_id):
        params = {
            'format': 'json',
            'user': user_id,
            'api_key': api_key,
            'method': 'user.gettopartists'
        }
        result = requests.get(lastfm_root_url, params=params).json()['topartists']['artist']
        result = list(map(strip_usseles_stuf, result))
        return result


class LastfmTopTags(Resource):
    def get(self, user_id):
        params = {
            'format': 'json',
            'user': user_id,
            'api_key': api_key,
            'method': 'user.gettoptags'
        }
        result = requests.get(lastfm_root_url, params=params).json()['toptags']['tag']
        result = list(map(strip_usseles_stuf, result))
        return result


api.add_resource(LastfmTopSongs, '/songs/<string:user_id>')
api.add_resource(LastfmTopSongs, '/artists/<string:user_id>')
api.add_resource(LastfmTopTags, '/tags/<string:user_id>')

# app.run(debug=True)
app.run(host='0.0.0.0', port=80)
